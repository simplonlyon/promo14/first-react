import { useState } from "react";
import { Greeting } from "./Greeting";


const intialState = {
    name: ''
};

export function NameList() {
    //Créer un state avec un tableau de noms dedans
    const [names, setNames] = useState(['Simplon', 'Jean', 'Bloup']);

    /**
     * le const form et le handleChange qu'on voit là, vous pouvez le réutiliser
     * pour un peu tous vos formulaires, il faut juste que les propriétés du state
     * form correspondent aux name des inputs du formulaire
     */
    const [form, setForm] = useState(intialState);

    const handleChange = event => {
        /**
         * Ici, on met à jour le state du formulaire au moment où on tape dans un input,
         * et pour savoir quelle propriété du state mettre à jour, on se base sur le
         * name de l'input actuel (event.target.name) et on lui assigne sa valeur (event.target.value)
         */

        setForm({
            ...form,
            [event.target.name]:event.target.value
        });
    }

    const handleSubmit = event => {
        event.preventDefault();
        /* Ici pour "pusher" une nouvelle dans le state, on va plutôt utiliser le spread
      operator à l'intérieur d'un nouveau tableau afin de ne pas affecter le state précédent */
        setNames([...names, form.name])
        setForm(intialState);
    }

    return (
        <section>
            {names.map((name, index) => <Greeting key={index} name={name} />)}
             <Greeting name={form.name ? form.name : '...'} />

            <form onSubmit={handleSubmit}>
                <label htmlFor="name">Name : </label>
                <input type="text" name="name" onChange={handleChange} value={form.name} />

                <button>Add</button>
            </form>
        </section>
    );
}