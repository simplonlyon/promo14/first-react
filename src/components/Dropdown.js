import { useState } from 'react';
import './Dropdown.css';



export function Dropdown({ choices }) {
    const [open, setOpen] = useState(false);
    const [choice, setChoice] = useState(null);

    const choose = (item) => {
        setChoice(item);
        setOpen(false);
    }

    return (

        <div className="dropdown">
            <p className="choice" onClick={() => setOpen(true)}>{choice ? choice:'----------'}<span>▼</span></p>
            {open && <ul>
                {choices.map((item, index) => <li onClick={() => choose(item)} key={index}>{item}</li>)}
            </ul>}
        </div>
    )
}