import { useState } from 'react';
import './Modal.css';

export function Modal({ children }) {

    const [open, setOpen] = useState(false);

    return (
        <>
            <button onClick={() => setOpen(true)}>Open Modal</button>
            {open &&
                <>
                    <div className="modalBackground" onClick={() => setOpen(false)}>
                    </div>

                    <div className="modal">
                        {/* La props "children" est une prop spéciale qui contiendra ce qu'on
                        aura mis entre les balises du component Modal au moment de l'appeler
                        (exemple, si je fais : <Modal><p>Bloup</p></Modal>, children contiendra <p>Bloup</p>) */}
                        {children}
                        <button className="close" onClick={() => setOpen(false)}>x</button>

                    </div>
                </>}
        </>
    );
}