import { useState } from "react";




export function Counter() {
    const [count, setCount] = useState(0); 

    const increment = () => {
        setCount(prev => prev+1);

    }
    const decrement = () => {
        setCount(count-1);
    }

    const reset = () => {
        setCount(0);
    }

    return (
        <section>
            <button onClick={decrement}>-</button>
            {count}
            <button onClick={increment}>+</button>
            <button onClick={reset}>Reset</button>
        </section>
    );
}