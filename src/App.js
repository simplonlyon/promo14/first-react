import './App.css';
import { Counter } from './components/Counter';
import { Dropdown } from './components/Dropdown';

import { Modal } from './components/Modal';
import { NameList } from './components/NameList';


function App() {

  

  return (
    <div>
      <h1>My First React App</h1>

      <Dropdown choices={['choix 1', 'choix 2', 'choix 3']} />
      <hr />
      <NameList />
      <hr/>

      <Counter />
      <hr/>

      <Modal>
        <p>Bloup</p>
      </Modal>
    </div>
  );
}

export default App;
